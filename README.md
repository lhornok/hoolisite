# hoolisite

Le site de la société Hooli est hébergé sur AWS dans un cluster Kubernertes qui est intégré à ce dépôt Gitlab.

Le dépôt chargé de la création de l'infrastructure se nomme "hooliinfra".

Le pipeline de ce dépôt se compose dans un premier temps par un déploiement dans le "namespace" de test du Cluster Kubernetes.

Puis dans un deuxième temps du déploiement dans le "namespace" de production du Cluster Kubernetes et la destricution 

## Déploiement dans le namespace test
Exécution de la commande kubectl apply -k k8/test avec le "namespace" test.
Puis avec ansible mount de l'EFS pour aller copier les fichiers dans php dans le répertoire test/myphp.

## Déploiement dans le namespace prod
Exécution de la commande kubectl apply -k k8/prod avec le "namespace" prod.
Puis avec ansible mount de l'EFS pour aller copier les fichiers dans php dans le répertoire prod/myphp.

## myphp.yaml
Création d'un service myphp qu'il est possible d'atteindre par le port 8082. Utilisation d'un volume persistant dans l'EFS.
Le docker utilisé est registry.gitlab.com/lhornok/hoolisite/myphp:latest.
Il un ingress qui redirige le traffice test/* vers la racine du site dans le conteneur.

## mysql.yaml
Création d'un service myphp-mysql, qui utilise l'image docker mysql:5.7.32. Utilisation d'un volume persistant pour les données dans /var/lib/mysql.

## kustomization.yaml
Ce sont les secrets pour Kubernetes